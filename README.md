# Starting the level(s)

Before starting each independent level, please run the following command to install necessary node modules.

```sh
yarn
```

You can then test that the level is successful through the available integration test.

```sh
yarn test:integration
```

For each level, the file coverage is also available in the coverage/ directory after running the following command

```sh
yarn test
```