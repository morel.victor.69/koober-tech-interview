"use strict";
exports.__esModule = true;
exports.Cart = void 0;
var Cart = /** @class */ (function () {
    function Cart(_a) {
        var id = _a.id;
        this.id = id;
        this.items = [];
        this.price = 0;
    }
    Cart.prototype.getPrice = function () {
        return this.price;
    };
    Cart.prototype.getId = function () {
        return this.id;
    };
    Cart.prototype.addItem = function (cartItem) {
        this.items.push(cartItem);
        this.price += cartItem.getPrice();
    };
    return Cart;
}());
exports.Cart = Cart;
