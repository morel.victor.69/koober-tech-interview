import { Cart } from './Cart';
import { buildMockCartWithItems, getMockCartPrice, mockCreateCartProps } from './Cart.spec.fixture';

describe('an e-commerce cart', () => {
  it('should successfully create a cart', () => {
    const mockCart = new Cart(mockCreateCartProps);
    expect(mockCart).toBeInstanceOf(Cart);
  });

  it('should successfully get the id of a cart', () => {
    const cart = new Cart(mockCreateCartProps);

    const expectedId = mockCreateCartProps.id;
    const actualId = cart.getId();

    expect(actualId).toStrictEqual(expectedId);
  });

  it('should successfully calculate the price of a cart without any items', () => {
    const emptyCart = new Cart(mockCreateCartProps);

    const expectedPrice = 0;
    const actualPrice = emptyCart.getPrice();

    expect(actualPrice).toStrictEqual(expectedPrice);
  });

  it('should successfully calculate the price of a cart with items', () => {
    const cart = buildMockCartWithItems();

    const expectedPrice = getMockCartPrice();
    const actualPrice = cart.getPrice();

    expect(actualPrice).toStrictEqual(expectedPrice);
  });
});