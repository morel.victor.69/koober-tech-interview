import { CartItem } from './CartItem';

export type CreateCartProps = {
  id: number;
};

export class Cart {
  private id: number;
  private items: CartItem[];
  private price: number;

  constructor({ id }: CreateCartProps) {
    this.id = id;
    this.items = [];
    this.price = 0;
  }

  public getPrice(): number {
    return this.price;
  }

  public getId(): number {
    return this.id;
  }

  public addItem(cartItem: CartItem): void {
    this.items.push(cartItem);
    this.price += cartItem.getPrice();
  }
}