import { mockArticle } from './Article.spec.fixture';
import { CartItem, CreateCartItemProps } from './CartItem';

export const mockCreateCartItemProps: CreateCartItemProps = {
  article: mockArticle,
  quantity: 6,
};

export const mockCartItem = new CartItem(mockCreateCartItemProps);