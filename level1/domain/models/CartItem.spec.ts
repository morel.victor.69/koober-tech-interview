import { CartItem } from './CartItem';
import { mockCreateCartItemProps } from './CartItem.spec.fixture';

describe('an e-commerce cart item', () => {
  it('should successfully create a cart item with a reference to an article, and the quantity of such article', () => {
    const cartItem = new CartItem(mockCreateCartItemProps);
    expect(cartItem).toBeInstanceOf(CartItem);
  });

  it('should successfully get the price of a cart item', () => {
    const cartItem = new CartItem(mockCreateCartItemProps);
    const expectedPrice = mockCreateCartItemProps.article.getPrice() * mockCreateCartItemProps.quantity;
    const actualPrice = cartItem.getPrice();

    expect(actualPrice).toStrictEqual(expectedPrice);
  });
});