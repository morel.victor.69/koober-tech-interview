"use strict";
exports.__esModule = true;
var Article_1 = require("./domain/models/Article");
var FileManager_1 = require("./infrastructure/FileManager");
var mappers_input_1 = require("./infrastructure/mappers.input");
var mappers_output_1 = require("./infrastructure/mappers.output");
function main() {
    var fileManager = new FileManager_1.FileManager();
    fileManager.readInputFile().then(function (input) {
        var articleInputs = input.articles, cartInputs = input.carts;
        var articles = Article_1.Article.buildFromListOfProps(articleInputs);
        var carts = (0, mappers_input_1.mapExternalCartsToDomainCarts)(cartInputs, articles);
        var output = (0, mappers_output_1.mapDomainCartsToExternalOutput)(carts);
        fileManager.buildOutputFile(output);
    });
}
if (require.main === module) {
    main();
}
