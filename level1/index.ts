import { Article } from './domain/models/Article';
import { Cart } from './domain/models/Cart';
import { FileManager } from './infrastructure/FileManager';
import { ExternalInput } from './infrastructure/FileManager.input.types';
import { ExternalOutput } from './infrastructure/FileManager.output.types';
import { mapExternalCartsToDomainCarts } from './infrastructure/mappers.input';
import { mapDomainCartsToExternalOutput } from './infrastructure/mappers.output';

function main() {
  const fileManager = new FileManager();
  fileManager.readInputFile().then((input: ExternalInput) => {
    const { articles: articleInputs, carts: cartInputs } = input;

    const articles: Article[] = Article.buildFromListOfProps(articleInputs);
    const carts: Cart[] = mapExternalCartsToDomainCarts(cartInputs, articles);

    const output: ExternalOutput = mapDomainCartsToExternalOutput(carts);

    fileManager.buildOutputFile(output);
  });
}

if (require.main === module) {
  main();
}