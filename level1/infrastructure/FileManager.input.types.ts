import { CreateArticleProps } from '../domain/models/Article';

export type ExternalInput = {
  articles: CreateArticleProps[];
  carts: ExternalInputCart[];
};

export type ExternalInputCart = {
  id: number;
  items: ExternalInputCartItem[];
};

export type ExternalInputCartItem = {
  article_id: number;
  quantity: number;
};