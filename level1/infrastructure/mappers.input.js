"use strict";
exports.__esModule = true;
exports.mapExternalCartItemToDomainCartItem = exports.mapExternalCartsToDomainCarts = void 0;
var Cart_1 = require("../domain/models/Cart");
var CartItem_1 = require("../domain/models/CartItem");
var mapExternalCartsToDomainCarts = function (cartInputs, articles) {
    return cartInputs.map(function (_a) {
        var id = _a.id, items = _a.items;
        var cart = new Cart_1.Cart({ id: id });
        items.forEach(function (externalInputCartItem) {
            var cartItem = (0, exports.mapExternalCartItemToDomainCartItem)(externalInputCartItem, articles);
            cart.addItem(cartItem);
        });
        return cart;
    });
};
exports.mapExternalCartsToDomainCarts = mapExternalCartsToDomainCarts;
var mapExternalCartItemToDomainCartItem = function (inputFileCartItem, articles) {
    var cartArticle = articles.find(function (article) {
        return article.getId() === inputFileCartItem.article_id;
    });
    if (cartArticle) {
        return new CartItem_1.CartItem({ article: cartArticle, quantity: inputFileCartItem.quantity });
    }
    throw new Error('cannot add in a cart an unexisting article');
};
exports.mapExternalCartItemToDomainCartItem = mapExternalCartItemToDomainCartItem;
