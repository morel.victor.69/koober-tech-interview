import { buildMockCartWithItems } from '../domain/models/Cart.spec.fixture';
import { mockCreateCartItemProps } from '../domain/models/CartItem.spec.fixture';
import { ExternalInputCart, ExternalInputCartItem } from './FileManager.input.types';

export const mockInputFileCartItem: ExternalInputCartItem = {
  article_id: mockCreateCartItemProps.article.getId(),
  quantity: mockCreateCartItemProps.quantity,
};

const mockCart = buildMockCartWithItems();

export const mockInputCarts: ExternalInputCart[] = [{ id: mockCart.getId(), items: [mockInputFileCartItem] }];