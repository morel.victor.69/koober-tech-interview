import { Cart } from '../domain/models/Cart';
import { ExternalOutput, ExternalOutputCart } from './FileManager.output.types';

export const mapDomainCartsToExternalOutput = (carts: Cart[]): ExternalOutput => {
  const outputCarts: ExternalOutputCart[] = carts.map((cart: Cart) => {
    return {
      id: cart.getId(),
      total: cart.getPrice(),
    };
  });
  const output: ExternalOutput = { carts: outputCarts };
  return output;
};