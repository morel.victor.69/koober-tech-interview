# Documentation

## File structure

I decided to split my application layers in two main folders, a domain folder and an infrastructure one.

The goal here is to store all domain-logic & related (types, interfaces, enums etc..) in the domain folder, whereas the infrastructure folder should handle all boundary-related logic (I/O, mapping external resources from/towards the domain). This allows the domain itself to remain pure and behave exactly as we expect it to, while keeping unwanted couplings/properties/etc.. outside of its realm.

The index file acts as a handler, and wraps all the logic together.

## Some architectural choices

1. I chose to loosely couple the `Article` and `CartItem` class through an interface because this will simplify further changes if they ever have to be implemented (dependency inversion principle).

2. I tried using TDD as much as possible when creating the domain, file manager, mappers. It's not always easy but overall, enforcing a good and clear test coverage helped with refactoring and file separation.

3. With the help of a decorator pattern, adding a dynamic delivery fee "property" to a cart allowed us to be quite modular in the process, and leaves place for further extension/modification : the base class was not modified, and we can, in the future, alter the existing behaviour quite easily (maybe we wish in the future to remove those delivery fees ? or add new ones ?)