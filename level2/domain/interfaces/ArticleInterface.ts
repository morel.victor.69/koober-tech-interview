export interface ArticleInterface {
  getPrice(): number;
  getId(): number;
}