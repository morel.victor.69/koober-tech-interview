"use strict";
exports.__esModule = true;
exports.Article = void 0;
var Article = /** @class */ (function () {
    function Article(_a) {
        var id = _a.id, price = _a.price, name = _a.name;
        this.id = id;
        this.price = price;
        this.name = name;
    }
    Article.prototype.getId = function () {
        return this.id;
    };
    Article.prototype.getPrice = function () {
        return this.price;
    };
    Article.buildFromListOfProps = function (propsList) {
        return propsList.map(function (props) {
            return new Article(props);
        });
    };
    return Article;
}());
exports.Article = Article;
