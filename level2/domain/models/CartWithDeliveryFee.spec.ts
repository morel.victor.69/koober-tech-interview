import { buildMockCartWithItems, getMockCartPrice } from './Cart.spec.fixture';
import { CartWithDeliveryFee } from './CartWithDeliveryFee';
import { mockCreateCartWithDeliveryFeeProps } from './CartWithDeliveryFee.spec.fixture';
import { mockDeliveryFee } from './DeliveryFee.spec.fixture';

describe('an e-commerce cart with a delivery fee applied to it', () => {
  it('should successfully create a cart with a delivery fee applied to it', () => {
    const cartWithDeliveryFee = new CartWithDeliveryFee(mockCreateCartWithDeliveryFeeProps);
    expect(cartWithDeliveryFee).toBeInstanceOf(CartWithDeliveryFee);
  });

  it('should successfully get the id of a cart with a delivery fee', () => {
    const cartWithDeliveryFee = new CartWithDeliveryFee(mockCreateCartWithDeliveryFeeProps);

    const expectedId = mockCreateCartWithDeliveryFeeProps.cart.getId();
    const actualId = cartWithDeliveryFee.getId();

    expect(actualId).toStrictEqual(expectedId);
  });

  it('should successfully add its delivery fee price to its cart price', () => {
    const cartWithDeliveryFee = new CartWithDeliveryFee(mockCreateCartWithDeliveryFeeProps);
    const expectedPrice = mockCreateCartWithDeliveryFeeProps.deliveryFee.applyFee(getMockCartPrice());
    const actualPrice = cartWithDeliveryFee.getPrice();
    expect(actualPrice).toStrictEqual(expectedPrice);
  });

  it('should successfully build a list of carts with delivery fees from a list of carts and a list of delivery fees', () => {
    const carts = [buildMockCartWithItems()];
    const deliveryFees = [mockDeliveryFee];
    const cartsWithDeliveryFees = CartWithDeliveryFee.buildFromListOfCartsAndDeliveryFees(carts, deliveryFees);
    expect(cartsWithDeliveryFees[0]).toBeInstanceOf(CartWithDeliveryFee);
  });
});