"use strict";
exports.__esModule = true;
exports.DeliveryFee = void 0;
var DeliveryFee = /** @class */ (function () {
    function DeliveryFee(_a) {
        var priceRange = _a.priceRange, price = _a.price;
        this.priceRange = priceRange;
        this.price = price;
    }
    DeliveryFee.prototype.checkIfFeeApplies = function (inputPrice) {
        return inputPrice >= this.priceRange.minPrice && inputPrice < this.priceRange.maxPrice;
    };
    DeliveryFee.prototype.applyFee = function (inputPrice) {
        return inputPrice + this.price;
    };
    return DeliveryFee;
}());
exports.DeliveryFee = DeliveryFee;
