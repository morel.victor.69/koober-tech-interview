export type PriceRange = {
  minPrice: number;
  maxPrice: number;
}