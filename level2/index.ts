import { Article } from './domain/models/Article';
import { Cart } from './domain/models/Cart';
import { CartWithDeliveryFee } from './domain/models/CartWithDeliveryFee';
import { DeliveryFee } from './domain/models/DeliveryFee';
import { FileManager } from './infrastructure/FileManager';
import { ExternalInput } from './infrastructure/FileManager.input.types';
import { ExternalOutput } from './infrastructure/FileManager.output.types';
import {
  mapExternalCartsToDomainCarts,
  mapExternalDeliveryFeesToDomainDeliveryFees,
} from './infrastructure/mappers.input';
import { mapDomainCartsToExternalOutput } from './infrastructure/mappers.output';

function main() {
  const fileManager = new FileManager();
  fileManager.readInputFile().then((input: ExternalInput) => {
    const { articles: articleInputs, carts: cartInputs, delivery_fees: deliveryFeeInputs } = input;

    const articles: Article[] = Article.buildFromListOfProps(articleInputs);
    const carts: Cart[] = mapExternalCartsToDomainCarts(cartInputs, articles);
    const deliveryFees: DeliveryFee[] = mapExternalDeliveryFeesToDomainDeliveryFees(deliveryFeeInputs);

    const cartsWithDeliveryFees: CartWithDeliveryFee[] = CartWithDeliveryFee.buildFromListOfCartsAndDeliveryFees(
      carts,
      deliveryFees
    );

    const output: ExternalOutput = mapDomainCartsToExternalOutput(cartsWithDeliveryFees);

    fileManager.buildOutputFile(output);
  });
}

if (require.main === module) {
  main();
}