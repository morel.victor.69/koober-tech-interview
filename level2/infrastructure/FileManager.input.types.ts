import { CreateArticleProps } from '../domain/models/Article';

export type ExternalInput = {
  articles: CreateArticleProps[];
  carts: ExternalInputCart[];
  delivery_fees: ExternalInputDeliveryFee[];
};

export type ExternalInputCart = {
  id: number;
  items: ExternalInputCartItem[];
};

export type ExternalInputCartItem = {
  article_id: number;
  quantity: number;
};

export type ExternalInputDeliveryFee = {
  price: number;
  eligible_transaction_volume: ExternalInputPriceRange;
};

export type ExternalInputPriceRange = {
  min_price: number;
  max_price: number | null;
};