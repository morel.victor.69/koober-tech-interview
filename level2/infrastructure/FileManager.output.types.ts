export type ExternalOutput = {
  carts: ExternalOutputCart[];
};

export type ExternalOutputCart = {
  id: number;
  total: number;
};