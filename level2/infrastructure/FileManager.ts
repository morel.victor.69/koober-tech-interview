import { promises as fsPromises } from 'fs';
import { ExternalInput } from './FileManager.input.types';
import { ExternalOutput } from './FileManager.output.types';

export class FileManager {
  private inputFilePath: string = 'input.json';
  private outputFilePath: string = 'output.json';

  public async readInputFile(): Promise<ExternalInput> {
    try {
      const fileAsString = await fsPromises.readFile(this.inputFilePath, 'binary');
      const result: ExternalInput = JSON.parse(fileAsString);
      return result;
    } catch (e) {
      throw e;
    }
  }

  public async buildOutputFile(challengeOutputs: ExternalOutput): Promise<void> {
    try {
      const outputAsString = JSON.stringify(challengeOutputs, null, 2);
      await fsPromises.writeFile(this.outputFilePath, outputAsString, 'binary');
    } catch (e) {
      throw e;
    }
  }
}