"use strict";
exports.__esModule = true;
exports.mapExternalDeliveryFeesToDomainDeliveryFees = exports.mapExternalCartItemToDomainCartItem = exports.mapExternalCartsToDomainCarts = void 0;
var Cart_1 = require("../domain/models/Cart");
var CartItem_1 = require("../domain/models/CartItem");
var DeliveryFee_1 = require("../domain/models/DeliveryFee");
var mapExternalCartsToDomainCarts = function (cartInputs, articles) {
    return cartInputs.map(function (_a) {
        var id = _a.id, items = _a.items;
        var cart = new Cart_1.Cart({ id: id });
        items.forEach(function (externalInputCartItem) {
            var cartItem = (0, exports.mapExternalCartItemToDomainCartItem)(externalInputCartItem, articles);
            cart.addItem(cartItem);
        });
        return cart;
    });
};
exports.mapExternalCartsToDomainCarts = mapExternalCartsToDomainCarts;
var mapExternalCartItemToDomainCartItem = function (inputFileCartItem, articles) {
    var cartArticle = articles.find(function (article) {
        return article.getId() === inputFileCartItem.article_id;
    });
    if (cartArticle) {
        return new CartItem_1.CartItem({ article: cartArticle, quantity: inputFileCartItem.quantity });
    }
    throw new Error('cannot add in a cart an unexisting article');
};
exports.mapExternalCartItemToDomainCartItem = mapExternalCartItemToDomainCartItem;
var mapExternalDeliveryFeesToDomainDeliveryFees = function (externalFees) {
    return externalFees.map(function (externalFee) {
        var _a;
        return new DeliveryFee_1.DeliveryFee({
            price: externalFee.price,
            priceRange: {
                minPrice: externalFee.eligible_transaction_volume.min_price,
                maxPrice: (_a = externalFee.eligible_transaction_volume.max_price) !== null && _a !== void 0 ? _a : Infinity
            }
        });
    });
};
exports.mapExternalDeliveryFeesToDomainDeliveryFees = mapExternalDeliveryFeesToDomainDeliveryFees;
