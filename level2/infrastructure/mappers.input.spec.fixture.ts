import { buildMockCartWithItems } from '../domain/models/Cart.spec.fixture';
import { mockCreateCartItemProps } from '../domain/models/CartItem.spec.fixture';
import { mockCreateDeliveryFeeProps } from '../domain/models/DeliveryFee.spec.fixture';
import { ExternalInputCart, ExternalInputCartItem, ExternalInputDeliveryFee } from './FileManager.input.types';

export const mockInputFileCartItem: ExternalInputCartItem = {
  article_id: mockCreateCartItemProps.article.getId(),
  quantity: mockCreateCartItemProps.quantity,
};

const mockCart = buildMockCartWithItems();

export const mockInputCarts: ExternalInputCart[] = [{ id: mockCart.getId(), items: [mockInputFileCartItem] }];

export const mockInputDeliveryFees: ExternalInputDeliveryFee[] = [
  {
    price: mockCreateDeliveryFeeProps.price,
    eligible_transaction_volume: {
      max_price: mockCreateDeliveryFeeProps.priceRange.maxPrice,
      min_price: mockCreateDeliveryFeeProps.priceRange.minPrice,
    },
  },
];