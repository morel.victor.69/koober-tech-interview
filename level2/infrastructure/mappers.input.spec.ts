import { mockArticles } from '../domain/models/Article.spec.fixture';
import { Cart } from '../domain/models/Cart';
import { buildMockCartWithItems } from '../domain/models/Cart.spec.fixture';
import { CartItem } from '../domain/models/CartItem';
import { mockCartItem } from '../domain/models/CartItem.spec.fixture';
import { DeliveryFee } from '../domain/models/DeliveryFee';
import { mockDeliveryFee } from '../domain/models/DeliveryFee.spec.fixture';
import {
  mapExternalCartItemToDomainCartItem,
  mapExternalCartsToDomainCarts,
  mapExternalDeliveryFeesToDomainDeliveryFees,
} from './mappers.input';
import { mockInputCarts, mockInputDeliveryFees, mockInputFileCartItem } from './mappers.input.spec.fixture';

describe('ensure the mappers between our domain and I/O boundaries behave as expected', () => {
  it('maps successfully a list of input file carts to a list of domain cart', () => {
    const expectedCarts: Cart[] = [buildMockCartWithItems()];
    const actualCarts: Cart[] = mapExternalCartsToDomainCarts(mockInputCarts, mockArticles);

    expect(actualCarts).toStrictEqual(expectedCarts);
  });

  it('maps successfully an input file cart item to a domain cart item', () => {
    const expectedCartItem: CartItem = mockCartItem;
    const actualCartItem: CartItem = mapExternalCartItemToDomainCartItem(mockInputFileCartItem, mockArticles);

    expect(actualCartItem).toStrictEqual(expectedCartItem);
  });

  it('throws an error if an input file cart item references an unexisting article', () => {
    const mapCartItems = () => {
      mapExternalCartItemToDomainCartItem(mockInputFileCartItem, []);
    };
    expect(mapCartItems).toThrowError(new Error('cannot add in a cart an unexisting article'));
  });

  it('maps successfully a list of input delivery fees to a list of domain delivery fees', () => {
    const expectedDeliveryFees: DeliveryFee[] = [mockDeliveryFee];
    const actualDeliveryFees: DeliveryFee[] = mapExternalDeliveryFeesToDomainDeliveryFees(mockInputDeliveryFees);

    expect(actualDeliveryFees).toStrictEqual(expectedDeliveryFees);
  });
});