import { ArticleInterface } from '../domain/interfaces/ArticleInterface';
import { Cart } from '../domain/models/Cart';
import { CartItem } from '../domain/models/CartItem';
import { DeliveryFee } from '../domain/models/DeliveryFee';
import { ExternalInputCart, ExternalInputCartItem, ExternalInputDeliveryFee } from './FileManager.input.types';

export const mapExternalCartsToDomainCarts = (
  cartInputs: ExternalInputCart[],
  articles: ArticleInterface[]
): Cart[] => {
  return cartInputs.map(({ id, items }: ExternalInputCart) => {
    const cart = new Cart({ id: id });
    items.forEach((externalInputCartItem: ExternalInputCartItem) => {
      const cartItem = mapExternalCartItemToDomainCartItem(externalInputCartItem, articles);
      cart.addItem(cartItem);
    });
    return cart;
  });
};

export const mapExternalCartItemToDomainCartItem = (
  inputFileCartItem: ExternalInputCartItem,
  articles: ArticleInterface[]
): CartItem => {
  const cartArticle = articles.find((article) => {
    return article.getId() === inputFileCartItem.article_id;
  });
  if (cartArticle) {
    return new CartItem({ article: cartArticle, quantity: inputFileCartItem.quantity });
  }
  throw new Error('cannot add in a cart an unexisting article');
};

export const mapExternalDeliveryFeesToDomainDeliveryFees = (
  externalFees: ExternalInputDeliveryFee[]
): DeliveryFee[] => {
  return externalFees.map((externalFee: ExternalInputDeliveryFee) => {
    return new DeliveryFee({
      price: externalFee.price,
      priceRange: {
        minPrice: externalFee.eligible_transaction_volume.min_price,
        maxPrice: externalFee.eligible_transaction_volume.max_price ?? Infinity,
      },
    });
  });
};