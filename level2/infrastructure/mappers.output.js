"use strict";
exports.__esModule = true;
exports.mapDomainCartsToExternalOutput = void 0;
var mapDomainCartsToExternalOutput = function (carts) {
    var outputCarts = carts.map(function (cart) {
        return {
            id: cart.getId(),
            total: cart.getPrice()
        };
    });
    var output = { carts: outputCarts };
    return output;
};
exports.mapDomainCartsToExternalOutput = mapDomainCartsToExternalOutput;
