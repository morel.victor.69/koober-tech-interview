import { CartInterface } from '../domain/interfaces/CartInterface';
import { buildMockCartWithItems, mockEmptyCart } from '../domain/models/Cart.spec.fixture';
import { ExternalOutput } from './FileManager.output.types';
import { mapDomainCartsToExternalOutput } from './mappers.output';

const cartWithItems = buildMockCartWithItems();
const emptyCart = mockEmptyCart;

const carts: CartInterface[] = [cartWithItems, emptyCart];

describe('ensure the output is correctly generated', () => {
  it('maps successfully the domain carts to the expected output object', () => {
    const expectedOutputs: ExternalOutput = {
      carts: [
        { id: 1, total: 600 },
        { id: 2, total: 0 },
      ],
    };
    const actualOutputs: ExternalOutput = mapDomainCartsToExternalOutput(carts);

    expect(actualOutputs).toStrictEqual(expectedOutputs);
  });
});