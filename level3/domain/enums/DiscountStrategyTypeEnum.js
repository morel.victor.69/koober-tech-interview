"use strict";
exports.__esModule = true;
exports.DiscountStrategyTypeEnum = void 0;
var DiscountStrategyTypeEnum;
(function (DiscountStrategyTypeEnum) {
    DiscountStrategyTypeEnum["PERCENTAGE"] = "percentage";
    DiscountStrategyTypeEnum["DIRECT_CUT"] = "amount";
})(DiscountStrategyTypeEnum = exports.DiscountStrategyTypeEnum || (exports.DiscountStrategyTypeEnum = {}));
