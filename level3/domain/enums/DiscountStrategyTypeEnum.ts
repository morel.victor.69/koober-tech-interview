export enum DiscountStrategyTypeEnum {
  PERCENTAGE = 'percentage',
  DIRECT_CUT = 'amount',
}