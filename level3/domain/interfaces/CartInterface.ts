export interface CartInterface {
  getId(): number;
  getPrice(): number;
}