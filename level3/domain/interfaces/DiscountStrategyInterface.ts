export interface DiscountStrategyInterface {
  applyDiscount(discountValue: number, inputPrice: number): number;
}