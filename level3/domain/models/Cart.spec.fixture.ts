import { Cart, CreateCartProps } from './Cart';
import { mockCartItem } from './CartItem.spec.fixture';

export const mockCreateCartProps: CreateCartProps = {
  id: 1,
};

export const mockCreateEmptyCartProps: CreateCartProps = {
  id: 2,
};

export const mockEmptyCart: Cart = new Cart(mockCreateEmptyCartProps);

export const mockCart: Cart = new Cart(mockCreateCartProps);

const mockCartItems = [mockCartItem];

export const buildMockCartWithItems = (): Cart => {
  const cart = new Cart(mockCreateCartProps);
  for (let mockCartItem of mockCartItems) {
    cart.addItem(mockCartItem);
  }
  return cart;
};

export const getMockCartPrice = (): number => {
  let mockCartPrice = 0;
  for (let mockCartItem of mockCartItems) {
    mockCartPrice += mockCartItem.getPrice();
  }
  return mockCartPrice;
};