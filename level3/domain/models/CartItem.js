"use strict";
exports.__esModule = true;
exports.CartItem = void 0;
var CartItem = /** @class */ (function () {
    function CartItem(_a) {
        var article = _a.article, quantity = _a.quantity;
        this.article = article;
        this.quantity = quantity;
    }
    CartItem.prototype.getPrice = function () {
        return this.article.getPrice() * this.quantity;
    };
    return CartItem;
}());
exports.CartItem = CartItem;
