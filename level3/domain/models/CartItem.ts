import { ArticleInterface } from '../interfaces/ArticleInterface';

export type CreateCartItemProps = {
  article: ArticleInterface;
  quantity: number;
};

export class CartItem {
  private article: ArticleInterface;
  private quantity: number;

  constructor({ article, quantity }: CreateCartItemProps) {
    this.article = article;
    this.quantity = quantity;
  }

  public getPrice(): number {
    return this.article.getPrice() * this.quantity;
  }
}