"use strict";
exports.__esModule = true;
exports.CartWithDeliveryFee = void 0;
var CartWithDeliveryFee = /** @class */ (function () {
    function CartWithDeliveryFee(_a) {
        var cart = _a.cart, deliveryFee = _a.deliveryFee;
        this.cart = cart;
        this.deliveryFee = deliveryFee;
    }
    CartWithDeliveryFee.prototype.getId = function () {
        return this.cart.getId();
    };
    CartWithDeliveryFee.prototype.getPrice = function () {
        var initialPrice = this.cart.getPrice();
        return this.deliveryFee.applyFee(initialPrice);
    };
    CartWithDeliveryFee.buildFromListOfCartsAndDeliveryFees = function (carts, deliveryFees) {
        return carts.map(function (cart) {
            var fittingDeliveryFee = deliveryFees.find(function (deliveryFee) {
                return deliveryFee.checkIfFeeApplies(cart.getPrice());
            });
            return new CartWithDeliveryFee({ cart: cart, deliveryFee: fittingDeliveryFee }); // we assume here we will always find a delivery fee fitting the cart's price
        });
    };
    return CartWithDeliveryFee;
}());
exports.CartWithDeliveryFee = CartWithDeliveryFee;
