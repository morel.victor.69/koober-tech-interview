import { buildMockCartWithItems } from './Cart.spec.fixture';
import { CreateCartWithDeliveryFeeProps } from './CartWithDeliveryFee';
import { mockDeliveryFee } from './DeliveryFee.spec.fixture';

export const mockCreateCartWithDeliveryFeeProps: CreateCartWithDeliveryFeeProps = {
  cart: buildMockCartWithItems(),
  deliveryFee: mockDeliveryFee,
};