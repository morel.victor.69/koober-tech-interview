import { CartInterface } from '../interfaces/CartInterface';
import { Cart } from './Cart';
import { DeliveryFee } from './DeliveryFee';

export type CreateCartWithDeliveryFeeProps = {
  cart: Cart;
  deliveryFee: DeliveryFee;
};

export class CartWithDeliveryFee implements CartInterface {
  private deliveryFee: DeliveryFee;
  private cart: Cart;

  constructor({ cart, deliveryFee }: CreateCartWithDeliveryFeeProps) {
    this.cart = cart;
    this.deliveryFee = deliveryFee;
  }

  public getId(): number {
    return this.cart.getId();
  }

  public getPrice(): number {
    const initialPrice = this.cart.getPrice();
    return this.deliveryFee.applyFee(initialPrice);
  }

  public static buildFromListOfCartsAndDeliveryFees(carts: Cart[], deliveryFees: DeliveryFee[]): CartWithDeliveryFee[] {
    return carts.map((cart: Cart) => {
      const fittingDeliveryFee = deliveryFees.find((deliveryFee: DeliveryFee) => {
        return deliveryFee.checkIfFeeApplies(cart.getPrice());
      });
      return new CartWithDeliveryFee({ cart: cart, deliveryFee: fittingDeliveryFee! }); // we assume here we will always find a delivery fee fitting the cart's price
    });
  }
}