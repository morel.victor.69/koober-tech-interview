import { PriceRange } from '../types/PriceRange';
import { CreateDeliveryFeeProps, DeliveryFee } from './DeliveryFee';

const mockPriceRange: PriceRange = { minPrice: 1000, maxPrice: 2000 };
export const mockCreateDeliveryFeeProps: CreateDeliveryFeeProps = { priceRange: mockPriceRange, price: 100 };

export const mockDeliveryFee: DeliveryFee = new DeliveryFee(mockCreateDeliveryFeeProps);