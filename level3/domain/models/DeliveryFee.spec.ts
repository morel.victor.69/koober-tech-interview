import { DeliveryFee } from './DeliveryFee';
import { mockCreateDeliveryFeeProps } from './DeliveryFee.spec.fixture';

describe('a delivery fee assigned to a cart', () => {
  it('should successfully create a delivery fee item, with min/max price range and a price reduction', () => {
    const deliveryFee = new DeliveryFee(mockCreateDeliveryFeeProps);
    expect(deliveryFee).toBeInstanceOf(DeliveryFee);
  });

  it('should return true if its fee applies on a received input price', () => {
    const deliveryFee = new DeliveryFee(mockCreateDeliveryFeeProps);
    const inputPrice = 1500;

    const expectedResult = true;
    const actualResult = deliveryFee.checkIfFeeApplies(inputPrice);

    expect(actualResult).toStrictEqual(expectedResult);
  });

  it('should return false if its fee does not apply on a received input price', () => {
    const deliveryFee = new DeliveryFee(mockCreateDeliveryFeeProps);
    const inputPrice = 3000;

    const expectedResult = false;
    const actualResult = deliveryFee.checkIfFeeApplies(inputPrice);

    expect(actualResult).toStrictEqual(expectedResult);
  });

  it('should apply its fee to an input price', () => {
    const deliveryFee = new DeliveryFee(mockCreateDeliveryFeeProps);
    const inputPrice = 2500;

    const expectedResult = 2600;
    const actualResult = deliveryFee.applyFee(inputPrice);

    expect(actualResult).toStrictEqual(expectedResult);
  });
});