import { PriceRange } from '../types/PriceRange';

export type CreateDeliveryFeeProps = {
  priceRange: PriceRange;
  price: number;
};

export class DeliveryFee {
  private priceRange: PriceRange;
  private price: number;

  constructor({ priceRange, price }: CreateDeliveryFeeProps) {
    this.priceRange = priceRange;
    this.price = price;
  }

  public checkIfFeeApplies(inputPrice: number): boolean {
    return inputPrice >= this.priceRange.minPrice && inputPrice < this.priceRange.maxPrice;
  }

  public applyFee(inputPrice: number): number {
    return inputPrice + this.price;
  }
}