import { Article, CreateArticleProps } from './Article';

export const mockCreateArticleProps: CreateArticleProps = {
  id: 1,
  price: 100,
  name: 'some-article',
};

export const mockCreateSecondArticleProps: CreateArticleProps = {
  id: 2,
  price: 250,
  name: 'some-other-article',
};

export const mockArticle = new Article(mockCreateArticleProps);
const mockSecondArticle = new Article(mockCreateSecondArticleProps);

export const mockArticles = [mockArticle, mockSecondArticle];