import { Article } from './Article';
import { mockCreateArticleProps, mockCreateSecondArticleProps } from './Article.spec.fixture';

describe('an e-commerce article', () => {
  it('should successfully create an article', () => {
    const article = new Article(mockCreateArticleProps);
    expect(article).toBeInstanceOf(Article);
  });

  it('should successfully get the price of an article', () => {
    const article = new Article(mockCreateArticleProps);
    const expectedPrice = mockCreateArticleProps.price;
    const actualPrice = article.getPrice();

    expect(actualPrice).toStrictEqual(expectedPrice);
  });

  it('should successfully get the id of an article', () => {
    const article = new Article(mockCreateArticleProps);

    const expectedId = mockCreateArticleProps.id;
    const actualId = article.getId();

    expect(actualId).toStrictEqual(expectedId);
  });

  it('should successfully create a list of articles from a list of inputs', () => {
    const mockArticles = Article.buildFromListOfProps([mockCreateArticleProps, mockCreateSecondArticleProps]);
    expect(mockArticles).toStrictEqual([
      new Article(mockCreateArticleProps),
      new Article(mockCreateSecondArticleProps),
    ]);
  });
});