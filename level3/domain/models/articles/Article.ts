import { ArticleInterface } from '../../interfaces/ArticleInterface';

export type CreateArticleProps = {
  id: number;
  name: string;
  price: number;
};

export class Article implements ArticleInterface {
  private id: number;
  private price: number;
  private name: string;

  constructor({ id, price, name }: CreateArticleProps) {
    this.id = id;
    this.price = price;
    this.name = name;
  }

  public getId(): number {
    return this.id;
  }

  public getPrice(): number {
    return this.price;
  }

  public static buildFromListOfProps(propsList: CreateArticleProps[]): Article[] {
    return propsList.map((props: CreateArticleProps) => {
      return new Article(props);
    });
  }
}