"use strict";
exports.__esModule = true;
exports.ArticleWithDiscount = void 0;
var ArticleWithDiscount = /** @class */ (function () {
    function ArticleWithDiscount(_a) {
        var article = _a.article, discount = _a.discount;
        this.article = article;
        this.discount = discount;
    }
    ArticleWithDiscount.prototype.getId = function () {
        return this.article.getId();
    };
    ArticleWithDiscount.prototype.getPrice = function () {
        var initialPrice = this.article.getPrice();
        return this.discount.applyDiscount(initialPrice);
    };
    return ArticleWithDiscount;
}());
exports.ArticleWithDiscount = ArticleWithDiscount;
