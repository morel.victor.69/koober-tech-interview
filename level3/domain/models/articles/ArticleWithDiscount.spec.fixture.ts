import { mockDiscount } from '../discounts/Discount.spec.fixture';
import { mockArticle } from './Article.spec.fixture';
import { ArticleWithDiscount } from './ArticleWithDiscount';

export const mockCreateArticleWithDiscountProps = {
  article: mockArticle,
  discount: mockDiscount,
};

export const mockArticleWithDiscount = new ArticleWithDiscount(mockCreateArticleWithDiscountProps);