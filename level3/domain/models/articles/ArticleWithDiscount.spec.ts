import { ArticleWithDiscount } from './ArticleWithDiscount';
import { mockCreateArticleWithDiscountProps } from './ArticleWithDiscount.spec.fixture';

describe('an article with a discount applied to it', () => {
  it('should successfully create an article with a discount applied to it', () => {
    const articleWithDiscount = new ArticleWithDiscount(mockCreateArticleWithDiscountProps);
    expect(articleWithDiscount).toBeInstanceOf(ArticleWithDiscount);
  });

  it('should successfully get the id of an article with a discount applied to it', () => {
    const articleWithDiscount = new ArticleWithDiscount(mockCreateArticleWithDiscountProps);

    const expectedId = mockCreateArticleWithDiscountProps.article.getId();
    const actualId = articleWithDiscount.getId();

    expect(actualId).toStrictEqual(expectedId);
  });

  it('should successfully apply its discount value to its article price', () => {
    const articleWithDiscount = new ArticleWithDiscount(mockCreateArticleWithDiscountProps);

    const expectedPrice = mockCreateArticleWithDiscountProps.discount.applyDiscount(
      mockCreateArticleWithDiscountProps.article.getPrice()
    );
    const actualPrice = articleWithDiscount.getPrice();
    expect(actualPrice).toStrictEqual(expectedPrice);
  });
});