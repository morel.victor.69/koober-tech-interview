import { ArticleInterface } from '../../interfaces/ArticleInterface';
import { Discount } from '../discounts/Discount';
import { Article } from './Article';

export type CreateArticleWithDiscountProps = {
  article: Article;
  discount: Discount;
};

export class ArticleWithDiscount implements ArticleInterface {
  private article: Article;
  private discount: Discount;

  constructor({ article, discount }: CreateArticleWithDiscountProps) {
    this.article = article;
    this.discount = discount;
  }

  public getId(): number {
    return this.article.getId();
  }

  public getPrice(): number {
    const initialPrice = this.article.getPrice();
    return this.discount.applyDiscount(initialPrice);
  }
}