"use strict";
exports.__esModule = true;
exports.DirectCutDiscountStrategy = void 0;
var DirectCutDiscountStrategy = /** @class */ (function () {
    function DirectCutDiscountStrategy() {
    }
    DirectCutDiscountStrategy.prototype.applyDiscount = function (discountValue, inputPrice) {
        return inputPrice - discountValue;
    };
    return DirectCutDiscountStrategy;
}());
exports.DirectCutDiscountStrategy = DirectCutDiscountStrategy;
