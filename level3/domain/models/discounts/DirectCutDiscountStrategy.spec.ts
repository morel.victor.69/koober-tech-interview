import { DirectCutDiscountStrategy } from './DirectCutDiscountStrategy';

describe('a discount strategy reducing the input price by direct cut', () => {
  it('should successfully create a direct cut discount strategy', () => {
    const directCutDiscountStrategy = new DirectCutDiscountStrategy();
    expect(directCutDiscountStrategy).toBeInstanceOf(DirectCutDiscountStrategy);
  });

  it('should successfully apply an input percentage discount to an input price', () => {
    const directCutDiscountStrategy = new DirectCutDiscountStrategy();
    const price = 200;
    const discountValue = 100;

    const expectedResult = 100;
    const actualResult = directCutDiscountStrategy.applyDiscount(discountValue, price);

    expect(actualResult).toStrictEqual(expectedResult);
  });
});