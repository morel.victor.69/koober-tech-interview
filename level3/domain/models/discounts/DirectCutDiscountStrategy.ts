import { DiscountStrategyInterface } from '../../interfaces/DiscountStrategyInterface';

export class DirectCutDiscountStrategy implements DiscountStrategyInterface {
  public applyDiscount(discountValue: number, inputPrice: number): number {
    return inputPrice - discountValue;
  }
}