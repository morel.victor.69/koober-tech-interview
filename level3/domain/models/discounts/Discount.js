"use strict";
exports.__esModule = true;
exports.Discount = void 0;
var Discount = /** @class */ (function () {
    function Discount(_a) {
        var value = _a.value, discountStrategy = _a.discountStrategy;
        this.value = value;
        this.discountStrategy = discountStrategy;
    }
    Discount.prototype.applyDiscount = function (inputPrice) {
        return this.discountStrategy.applyDiscount(this.value, inputPrice);
    };
    return Discount;
}());
exports.Discount = Discount;
