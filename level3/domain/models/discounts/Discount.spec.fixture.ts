import { DirectCutDiscountStrategy } from './DirectCutDiscountStrategy';
import { CreateDiscountProps, Discount } from './Discount';

export const mockCreateDiscountProps: CreateDiscountProps = {
  value: 500,
  discountStrategy: new DirectCutDiscountStrategy(),
};

export const mockDiscount = new Discount(mockCreateDiscountProps);