import { Discount } from './Discount';
import { mockCreateDiscountProps } from './Discount.spec.fixture';

describe('a discount applied to an article', () => {
  it('should successfully create a discount', () => {
    const discount = new Discount(mockCreateDiscountProps);
    expect(discount).toBeInstanceOf(Discount);
  });

  it('should successfully apply its discount to an input price', () => {
    const discount = new Discount(mockCreateDiscountProps);
    const priceBeforeDiscount = 200;

    const expectedPrice = mockCreateDiscountProps.discountStrategy.applyDiscount(
      mockCreateDiscountProps.value,
      priceBeforeDiscount
    );
    const actualPrice = discount.applyDiscount(priceBeforeDiscount);

    expect(actualPrice).toStrictEqual(expectedPrice);
  });
});