import { DiscountStrategyInterface } from '../../interfaces/DiscountStrategyInterface';

export type CreateDiscountProps = {
  value: number;
  discountStrategy: DiscountStrategyInterface;
};

export class Discount {
  private value: number;
  private discountStrategy: DiscountStrategyInterface;

  constructor({ value, discountStrategy }: CreateDiscountProps) {
    this.value = value;
    this.discountStrategy = discountStrategy;
  }

  public applyDiscount(inputPrice: number): number {
    return this.discountStrategy.applyDiscount(this.value, inputPrice);
  }
}