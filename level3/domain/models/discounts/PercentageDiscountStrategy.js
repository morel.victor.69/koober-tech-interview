"use strict";
exports.__esModule = true;
exports.PercentageDiscountStrategy = void 0;
var PercentageDiscountStrategy = /** @class */ (function () {
    function PercentageDiscountStrategy() {
    }
    PercentageDiscountStrategy.prototype.applyDiscount = function (discountValue, inputPrice) {
        return Math.floor((1 - discountValue / 100) * inputPrice); // We use Math.floor here to keep integer numbers, and probably because we will always prefer smaller discounts rather than big ones -- no Math.ceil :)
    };
    return PercentageDiscountStrategy;
}());
exports.PercentageDiscountStrategy = PercentageDiscountStrategy;
