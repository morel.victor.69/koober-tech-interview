import { PercentageDiscountStrategy } from './PercentageDiscountStrategy';

describe('a discount strategy reducing the input price by percentage', () => {
  it('should successfully create a percentage discount strategy', () => {
    const percentageDiscountStrategy = new PercentageDiscountStrategy();
    expect(percentageDiscountStrategy).toBeInstanceOf(PercentageDiscountStrategy);
  });

  it('should successfully apply an input percentage discount to an input price', () => {
    const percentageDiscountStrategy = new PercentageDiscountStrategy();
    const price = 200;
    const discountPercentage = 30;

    const expectedResult = 140;
    const actualResult = percentageDiscountStrategy.applyDiscount(discountPercentage, price);

    expect(actualResult).toStrictEqual(expectedResult);
  });
});