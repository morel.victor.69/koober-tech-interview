import { DiscountStrategyInterface } from '../../interfaces/DiscountStrategyInterface';

export class PercentageDiscountStrategy implements DiscountStrategyInterface {
  public applyDiscount(discountValue: number, inputPrice: number): number {
    return Math.floor((1 - discountValue / 100) * inputPrice); // We use Math.floor here to keep integer numbers, and probably because we will always prefer smaller discounts rather than big ones -- no Math.ceil :)
  }
}