"use strict";
exports.__esModule = true;
var Article_1 = require("./domain/models/articles/Article");
var CartWithDeliveryFee_1 = require("./domain/models/CartWithDeliveryFee");
var FileManager_1 = require("./infrastructure/FileManager");
var mappers_input_1 = require("./infrastructure/mappers.input");
var mappers_output_1 = require("./infrastructure/mappers.output");
function main() {
    var fileManager = new FileManager_1.FileManager();
    fileManager.readInputFile().then(function (input) {
        var articleInputs = input.articles, cartInputs = input.carts, deliveryFeeInputs = input.delivery_fees, discountInputs = input.discounts;
        var articles = Article_1.Article.buildFromListOfProps(articleInputs);
        var articlesWithDiscounts = (0, mappers_input_1.mapExternalDiscountsToDomainArticles)(articles, discountInputs);
        var carts = (0, mappers_input_1.mapExternalCartsToDomainCarts)(cartInputs, articlesWithDiscounts);
        var deliveryFees = (0, mappers_input_1.mapExternalDeliveryFeesToDomainDeliveryFees)(deliveryFeeInputs);
        var cartsWithDeliveryFees = CartWithDeliveryFee_1.CartWithDeliveryFee.buildFromListOfCartsAndDeliveryFees(carts, deliveryFees);
        var output = (0, mappers_output_1.mapDomainCartsToExternalOutput)(cartsWithDeliveryFees);
        fileManager.buildOutputFile(output);
    });
}
if (require.main === module) {
    main();
}
