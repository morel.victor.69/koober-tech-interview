import { DiscountStrategyTypeEnum } from '../domain/enums/DiscountStrategyTypeEnum';
import { CreateArticleProps } from '../domain/models/articles/Article';

export type ExternalInput = {
  articles: CreateArticleProps[];
  carts: ExternalInputCart[];
  delivery_fees: ExternalInputDeliveryFee[];
  discounts: ExternalInputDiscount[];
};

export type ExternalInputCart = {
  id: number;
  items: ExternalInputCartItem[];
};

export type ExternalInputCartItem = {
  article_id: number;
  quantity: number;
};

export type ExternalInputDeliveryFee = {
  price: number;
  eligible_transaction_volume: ExternalInputPriceRange;
};

export type ExternalInputPriceRange = {
  min_price: number;
  max_price: number | null;
};

export type ExternalInputDiscount = {
  article_id: number;
  type: DiscountStrategyTypeEnum;
  value: number;
};