import { DiscountStrategyTypeEnum } from '../domain/enums/DiscountStrategyTypeEnum';
import { FileManager } from './FileManager';
import { ExternalInput } from './FileManager.input.types';
import { ExternalOutput } from './FileManager.output.types';

const mockReadFileFn = jest.fn();
const mockWriteFileFn = jest.fn();

const mockChallengeOutputs: ExternalOutput = { carts: [{ id: 1, total: 100 }] };

jest.mock('fs', () => {
  return {
    promises: {
      readFile: () => mockReadFileFn(),
      writeFile: (...args: any) => mockWriteFileFn(...args),
    },
  };
});

describe("a file manager which can be used to parse the challenge's input file, and create the challenge's output file", () => {
  it("successfully parses the challenge's input file into a local variable", async () => {
    const expectedResult = getExpectedResult();
    mockReadFileFn.mockReturnValueOnce(JSON.stringify(expectedResult));

    const fileManager = new FileManager();
    const actualResult = await fileManager.readInputFile();

    expect(actualResult).toStrictEqual(expectedResult);
  });

  it("throws an error when it fails to parse the challenge's input file", async () => {
    mockReadFileFn.mockRejectedValueOnce(() => {
      throw new Error();
    });

    const readFile = async () => {
      const fileManager = new FileManager();
      await fileManager.readInputFile();
    };

    await expect(readFile()).rejects.toThrowError();
  });

  it("successfully creates the challenge's output file", async () => {
    const expectedGeneratedFileAsString = JSON.stringify(mockChallengeOutputs, null, 2);
    const fileManager = new FileManager();

    await fileManager.buildOutputFile(mockChallengeOutputs);

    expect(mockWriteFileFn).toHaveBeenNthCalledWith(1, 'output.json', expectedGeneratedFileAsString, 'binary');
  });

  it("throws an error when failing to create the challenge's output file", async () => {
    mockWriteFileFn.mockRejectedValueOnce(() => {
      throw new Error();
    });

    const writeFile = async () => {
      const fileManager = new FileManager();
      await fileManager.buildOutputFile(mockChallengeOutputs);
    };

    await expect(writeFile()).rejects.toThrowError();
  });
});

const getExpectedResult = (): ExternalInput => {
  const expectedResult: ExternalInput = {
    articles: [
      { id: 1, name: 'water', price: 100 },
      { id: 2, name: 'honey', price: 200 },
      { id: 3, name: 'mango', price: 400 },
      { id: 4, name: 'tea', price: 1000 },
    ],
    carts: [
      {
        id: 1,
        items: [
          { article_id: 1, quantity: 6 },
          { article_id: 2, quantity: 2 },
          { article_id: 4, quantity: 1 },
        ],
      },
      {
        id: 2,
        items: [
          { article_id: 2, quantity: 1 },
          { article_id: 3, quantity: 3 },
        ],
      },
      { id: 3, items: [] },
    ],
    delivery_fees: [
      {
        eligible_transaction_volume: {
          min_price: 0,
          max_price: 1000,
        },
        price: 800,
      },
      {
        eligible_transaction_volume: {
          min_price: 1000,
          max_price: 2000,
        },
        price: 400,
      },
      {
        eligible_transaction_volume: {
          min_price: 2000,
          max_price: null,
        },
        price: 0,
      },
    ],
    discounts: [
      {
        article_id: 2,
        type: DiscountStrategyTypeEnum.DIRECT_CUT,
        value: 25,
      },
      {
        article_id: 5,
        type: DiscountStrategyTypeEnum.PERCENTAGE,

        value: 30,
      },
      {
        article_id: 6,
        type: DiscountStrategyTypeEnum.PERCENTAGE,
        value: 30,
      },
      {
        article_id: 7,
        type: DiscountStrategyTypeEnum.PERCENTAGE,
        value: 25,
      },
      {
        article_id: 8,
        type: DiscountStrategyTypeEnum.PERCENTAGE,
        value: 10,
      },
    ],
  };
  return expectedResult;
};