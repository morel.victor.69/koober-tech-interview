"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
exports.__esModule = true;
exports.mapExternalDiscountStrategyToDomainDiscountStrategy = exports.mapExternalDiscountsToDomainArticles = exports.mapExternalDeliveryFeesToDomainDeliveryFees = exports.mapExternalCartItemToDomainCartItem = exports.mapExternalCartsToDomainCarts = void 0;
var DiscountStrategyTypeEnum_1 = require("../domain/enums/DiscountStrategyTypeEnum");
var ArticleWithDiscount_1 = require("../domain/models/articles/ArticleWithDiscount");
var Cart_1 = require("../domain/models/Cart");
var CartItem_1 = require("../domain/models/CartItem");
var DeliveryFee_1 = require("../domain/models/DeliveryFee");
var DirectCutDiscountStrategy_1 = require("../domain/models/discounts/DirectCutDiscountStrategy");
var Discount_1 = require("../domain/models/discounts/Discount");
var PercentageDiscountStrategy_1 = require("../domain/models/discounts/PercentageDiscountStrategy");
var mapExternalCartsToDomainCarts = function (cartInputs, articles) {
    return cartInputs.map(function (_a) {
        var id = _a.id, items = _a.items;
        var cart = new Cart_1.Cart({ id: id });
        items.forEach(function (externalInputCartItem) {
            var cartItem = (0, exports.mapExternalCartItemToDomainCartItem)(externalInputCartItem, articles);
            cart.addItem(cartItem);
        });
        return cart;
    });
};
exports.mapExternalCartsToDomainCarts = mapExternalCartsToDomainCarts;
var mapExternalCartItemToDomainCartItem = function (inputFileCartItem, articles) {
    var cartArticle = articles.find(function (article) {
        return article.getId() === inputFileCartItem.article_id;
    });
    if (cartArticle) {
        return new CartItem_1.CartItem({ article: cartArticle, quantity: inputFileCartItem.quantity });
    }
    throw new Error('cannot add in a cart an unexisting article');
};
exports.mapExternalCartItemToDomainCartItem = mapExternalCartItemToDomainCartItem;
var mapExternalDeliveryFeesToDomainDeliveryFees = function (externalFees) {
    return externalFees.map(function (externalFee) {
        var _a;
        return new DeliveryFee_1.DeliveryFee({
            price: externalFee.price,
            priceRange: {
                minPrice: externalFee.eligible_transaction_volume.min_price,
                maxPrice: (_a = externalFee.eligible_transaction_volume.max_price) !== null && _a !== void 0 ? _a : Infinity
            }
        });
    });
};
exports.mapExternalDeliveryFeesToDomainDeliveryFees = mapExternalDeliveryFeesToDomainDeliveryFees;
var mapExternalDiscountsToDomainArticles = function (articles, externalDiscounts) {
    var articlesAfterDiscountsAreApplied = __spreadArray([], articles, true);
    var _loop_1 = function (externalDiscount) {
        var discount = new Discount_1.Discount({
            value: externalDiscount.value,
            discountStrategy: (0, exports.mapExternalDiscountStrategyToDomainDiscountStrategy)(externalDiscount.type)
        });
        var articleToWhichDiscountAppliesIndex = articles.findIndex(function (article) {
            return article.getId() === externalDiscount.article_id;
        });
        var articleToWhichDiscountApplies = articles[articleToWhichDiscountAppliesIndex];
        var articleWithDiscount = new ArticleWithDiscount_1.ArticleWithDiscount({ article: articleToWhichDiscountApplies, discount: discount });
        articlesAfterDiscountsAreApplied[articleToWhichDiscountAppliesIndex] = articleWithDiscount;
    };
    for (var _i = 0, externalDiscounts_1 = externalDiscounts; _i < externalDiscounts_1.length; _i++) {
        var externalDiscount = externalDiscounts_1[_i];
        _loop_1(externalDiscount);
    }
    return articlesAfterDiscountsAreApplied;
};
exports.mapExternalDiscountsToDomainArticles = mapExternalDiscountsToDomainArticles;
var mapExternalDiscountStrategyToDomainDiscountStrategy = function (externalDiscountStrategy) {
    switch (externalDiscountStrategy) {
        case DiscountStrategyTypeEnum_1.DiscountStrategyTypeEnum.DIRECT_CUT:
            return new DirectCutDiscountStrategy_1.DirectCutDiscountStrategy();
        case DiscountStrategyTypeEnum_1.DiscountStrategyTypeEnum.PERCENTAGE:
            return new PercentageDiscountStrategy_1.PercentageDiscountStrategy();
        default:
            throw new Error('unknown discount strategy provided');
    }
};
exports.mapExternalDiscountStrategyToDomainDiscountStrategy = mapExternalDiscountStrategyToDomainDiscountStrategy;
