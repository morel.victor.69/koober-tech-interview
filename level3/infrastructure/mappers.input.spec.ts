import { ArticleInterface } from '../domain/interfaces/ArticleInterface';
import { mockArticle, mockArticles } from '../domain/models/articles/Article.spec.fixture';
import { mockArticleWithDiscount } from '../domain/models/articles/ArticleWithDiscount.spec.fixture';
import { Cart } from '../domain/models/Cart';
import { buildMockCartWithItems } from '../domain/models/Cart.spec.fixture';
import { CartItem } from '../domain/models/CartItem';
import { mockCartItem } from '../domain/models/CartItem.spec.fixture';
import { DeliveryFee } from '../domain/models/DeliveryFee';
import { mockDeliveryFee } from '../domain/models/DeliveryFee.spec.fixture';
import { DirectCutDiscountStrategy } from '../domain/models/discounts/DirectCutDiscountStrategy';
import { PercentageDiscountStrategy } from '../domain/models/discounts/PercentageDiscountStrategy';
import {
  mapExternalCartItemToDomainCartItem,
  mapExternalCartsToDomainCarts,
  mapExternalDeliveryFeesToDomainDeliveryFees,
  mapExternalDiscountsToDomainArticles,
  mapExternalDiscountStrategyToDomainDiscountStrategy,
} from './mappers.input';
import {
  mockInputCarts,
  mockInputDeliveryFees,
  mockInputFileCartItem,
  mockExternalInputDirectCutDiscount,
  mockExternalInputPercentageDiscount,
} from './mappers.input.spec.fixture';

describe('ensure the mappers between our domain and I/O boundaries behave as expected', () => {
  it('maps successfully a list of input file carts to a list of domain cart', () => {
    const expectedCarts: Cart[] = [buildMockCartWithItems()];
    const actualCarts: Cart[] = mapExternalCartsToDomainCarts(mockInputCarts, mockArticles);

    expect(actualCarts).toStrictEqual(expectedCarts);
  });

  it('maps successfully an input file cart item to a domain cart item', () => {
    const expectedCartItem: CartItem = mockCartItem;
    const actualCartItem: CartItem = mapExternalCartItemToDomainCartItem(mockInputFileCartItem, mockArticles);

    expect(actualCartItem).toStrictEqual(expectedCartItem);
  });

  it('throws an error if an input file cart item references an unexisting article', () => {
    const mapCartItems = () => {
      mapExternalCartItemToDomainCartItem(mockInputFileCartItem, []);
    };
    expect(mapCartItems).toThrowError(new Error('cannot add in a cart an unexisting article'));
  });

  it('maps successfully a list of input delivery fees to a list of domain delivery fees', () => {
    const expectedDeliveryFees: DeliveryFee[] = [mockDeliveryFee];
    const actualDeliveryFees: DeliveryFee[] = mapExternalDeliveryFeesToDomainDeliveryFees(mockInputDeliveryFees);

    expect(actualDeliveryFees).toStrictEqual(expectedDeliveryFees);
  });

  it('maps successfully a list of input file discounts to a list of domain articles', () => {
    const expectedArticles = [mockArticleWithDiscount];
    const actualArticles: ArticleInterface[] = mapExternalDiscountsToDomainArticles(
      [mockArticle],
      [mockExternalInputDirectCutDiscount]
    );

    expect(actualArticles).toStrictEqual(expectedArticles);
  });

  it('maps successfully an external discount strategy to a domain discount strategy', () => {
    const expectedDirectCutDiscountStrategy = new DirectCutDiscountStrategy();
    const actualDirectCutDiscountStrategy = mapExternalDiscountStrategyToDomainDiscountStrategy(
      mockExternalInputDirectCutDiscount.type
    );
    expect(actualDirectCutDiscountStrategy).toStrictEqual(expectedDirectCutDiscountStrategy);

    const expectedPercentageDiscountStrategy = new PercentageDiscountStrategy();
    const actualPercentageDiscountStrategy = mapExternalDiscountStrategyToDomainDiscountStrategy(
      mockExternalInputPercentageDiscount.type
    );
    expect(actualPercentageDiscountStrategy).toStrictEqual(expectedPercentageDiscountStrategy);
  });
});