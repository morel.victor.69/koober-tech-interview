import { DiscountStrategyTypeEnum } from '../domain/enums/DiscountStrategyTypeEnum';
import { ArticleInterface } from '../domain/interfaces/ArticleInterface';
import { DiscountStrategyInterface } from '../domain/interfaces/DiscountStrategyInterface';
import { Article } from '../domain/models/articles/Article';
import { ArticleWithDiscount } from '../domain/models/articles/ArticleWithDiscount';
import { Cart } from '../domain/models/Cart';
import { CartItem } from '../domain/models/CartItem';
import { DeliveryFee } from '../domain/models/DeliveryFee';
import { DirectCutDiscountStrategy } from '../domain/models/discounts/DirectCutDiscountStrategy';
import { Discount } from '../domain/models/discounts/Discount';
import { PercentageDiscountStrategy } from '../domain/models/discounts/PercentageDiscountStrategy';
import {
  ExternalInputCart,
  ExternalInputCartItem,
  ExternalInputDeliveryFee,
  ExternalInputDiscount,
} from './FileManager.input.types';

export const mapExternalCartsToDomainCarts = (
  cartInputs: ExternalInputCart[],
  articles: ArticleInterface[]
): Cart[] => {
  return cartInputs.map(({ id, items }: ExternalInputCart) => {
    const cart = new Cart({ id: id });
    items.forEach((externalInputCartItem: ExternalInputCartItem) => {
      const cartItem = mapExternalCartItemToDomainCartItem(externalInputCartItem, articles);
      cart.addItem(cartItem);
    });
    return cart;
  });
};

export const mapExternalCartItemToDomainCartItem = (
  inputFileCartItem: ExternalInputCartItem,
  articles: ArticleInterface[]
): CartItem => {
  const cartArticle = articles.find((article) => {
    return article.getId() === inputFileCartItem.article_id;
  });
  if (cartArticle) {
    return new CartItem({ article: cartArticle, quantity: inputFileCartItem.quantity });
  }
  throw new Error('cannot add in a cart an unexisting article');
};

export const mapExternalDeliveryFeesToDomainDeliveryFees = (
  externalFees: ExternalInputDeliveryFee[]
): DeliveryFee[] => {
  return externalFees.map((externalFee: ExternalInputDeliveryFee) => {
    return new DeliveryFee({
      price: externalFee.price,
      priceRange: {
        minPrice: externalFee.eligible_transaction_volume.min_price,
        maxPrice: externalFee.eligible_transaction_volume.max_price ?? Infinity,
      },
    });
  });
};

export const mapExternalDiscountsToDomainArticles = (
  articles: Article[],
  externalDiscounts: ExternalInputDiscount[]
): ArticleInterface[] => {
  const articlesAfterDiscountsAreApplied: (Article | ArticleWithDiscount)[] = [...articles];
  for (let externalDiscount of externalDiscounts) {
    const discount = new Discount({
      value: externalDiscount.value,
      discountStrategy: mapExternalDiscountStrategyToDomainDiscountStrategy(externalDiscount.type),
    });

    const articleToWhichDiscountAppliesIndex = articles.findIndex((article: Article) => {
      return article.getId() === externalDiscount.article_id;
    });
    const articleToWhichDiscountApplies = articles[articleToWhichDiscountAppliesIndex];
    const articleWithDiscount = new ArticleWithDiscount({ article: articleToWhichDiscountApplies, discount: discount });

    articlesAfterDiscountsAreApplied[articleToWhichDiscountAppliesIndex] = articleWithDiscount;
  }
  return articlesAfterDiscountsAreApplied;
};

export const mapExternalDiscountStrategyToDomainDiscountStrategy = (
  externalDiscountStrategy: DiscountStrategyTypeEnum
): DiscountStrategyInterface => {
  switch (externalDiscountStrategy) {
    case DiscountStrategyTypeEnum.DIRECT_CUT:
      return new DirectCutDiscountStrategy();
    case DiscountStrategyTypeEnum.PERCENTAGE:
      return new PercentageDiscountStrategy();
    default:
      throw new Error('unknown discount strategy provided');
  }
};