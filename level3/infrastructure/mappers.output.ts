import { CartInterface } from '../domain/interfaces/CartInterface';
import { ExternalOutput, ExternalOutputCart } from './FileManager.output.types';

export const mapDomainCartsToExternalOutput = (carts: CartInterface[]): ExternalOutput => {
  const outputCarts: ExternalOutputCart[] = carts.map((cart: CartInterface) => {
    return {
      id: cart.getId(),
      total: cart.getPrice(),
    };
  });
  const output: ExternalOutput = { carts: outputCarts };
  return output;
};